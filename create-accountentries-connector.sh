#!/bin/sh
#
# Use this script to create the JDBC Connector to read account entries
#
docker-compose exec connect \
curl -X POST -H "Content-Type: application/json" http://connect:8085/connectors --data @/connectorConfig/jdbc-accountentries.json
