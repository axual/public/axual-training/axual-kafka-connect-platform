#!/bin/bash
#
# Use this sript to read events from the "interaction-alert" topic using the console consumer
#

docker-compose exec broker \
  kafka-topics --list --zookeeper zookeeper:2181
