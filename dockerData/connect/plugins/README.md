# Plugin Directory Structure
Each jar in the plugin directory will be loaded into isolated classloaders.
If a plugin requires multiple jars, add them to a subdirectory of the plugin directory