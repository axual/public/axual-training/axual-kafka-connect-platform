CREATE DATABASE `connect` COLLATE 'utf8_unicode_ci';
USE  `connect`;

CREATE OR REPLACE TABLE `authorizations` (
    `account` varchar(34) NOT NULL PRIMARY KEY ,
    `modified` timestamp NOT NULL,
    `user` varchar(50) NOT NULL
);

CREATE OR REPLACE
    TRIGGER `authorizations_preinsert`
    BEFORE INSERT ON `authorizations` FOR EACH ROW
    SET new.`modified` = CURRENT_TIMESTAMP();

CREATE OR REPLACE
    TRIGGER `authorizations_preupdate`
    BEFORE UPDATE ON `authorizations` FOR EACH ROW
    SET new.`modified` = CURRENT_TIMESTAMP();

CREATE OR REPLACE TABLE `accountentries` (
    `identifier` bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `modified` timestamp NOT NULL,
    `account` varchar(34) NOT NULL,
    `currency` varchar(20) NOT NULL DEFAULT 'euro',
    `amount` double(15,2) UNSIGNED NOT NULL,
    `balanceAfterTransaction` double(15,2) SIGNED NOT NULL,
    `transactionType` enum( 'DEBIT', 'CREDIT') NOT NULL,
    `description` varchar(255) NOT NULL,
    `counterpartyAccount` varchar(34) NOT NULL,
    `counterpartyName` varchar(50) NOT NULL
);

CREATE OR REPLACE
    TRIGGER `accountentries_preinsert`
    BEFORE INSERT ON `accountentries` FOR EACH ROW
    SET new.`modified` = CURRENT_TIMESTAMP();

CREATE OR REPLACE
    TRIGGER `accountentries_preupdate`
    BEFORE UPDATE ON `accountentries` FOR EACH ROW
    SET new.`modified` = CURRENT_TIMESTAMP();
